﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MyBlog.Models
{
    public class PostModel
    {
        public int ID { get; set; }
        public string Username { get; set; }
        public string Title { get; set; }
        public string Image { get; set; }
        public string Genre { get; set; }
        public DateTime Date { get; set; }
        public string Summary { get; set; }
        public string Content { get; set; }

        public List<CommentModel> Comments { get; set; }

        public PostModel(string username, string title, string image, string genre, DateTime date, string summary, string content, List<CommentModel> comments)
        {
            Username = username;
            Title = title;
            Image = image;
            Genre = genre;
            Date = date;
            Summary = summary;
            Content = content;
            Comments = comments;
        }

        public PostModel(string username, string title, string image, string genre, DateTime date, string summary, string content)
        {
            Username = username;
            Title = title;
            Image = image;
            Genre = genre;
            Date = date;
            Summary = summary;
            Content = content;
            Comments = new List<CommentModel>();
        }

        public PostModel()
        {
        }
    }
}
