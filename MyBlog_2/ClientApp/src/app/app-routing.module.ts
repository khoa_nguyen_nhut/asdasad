import { BlogPostDetailComponent } from './blog-post-detail/blog-post-detail.component';
import { BlogPostListComponent } from './blog-post-list/blog-post-list.component';
import { AboutMeComponent } from './about-me/about-me.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  { path: '', component: BlogPostListComponent},
  { path: 'about-me', component: AboutMeComponent},
  { path: 'posts/:postid', component: BlogPostDetailComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
