﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MyBlog.Data;
using MyBlog.Models;

namespace MyBlog_2.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PostController : ControllerBase
    {
        private readonly MyBlogContext _blogContext;

        public PostController(MyBlogContext blogContext)
        {
            _blogContext = blogContext;
        }

        // GET: api/Post
        [HttpGet]
        public IEnumerable<PostModel> Get()
        {
            var PostList = _blogContext.Posts.Include(post => post.Comments).OrderByDescending(Post => Post.Date).ToList();
            return PostList;
        }

        // GET: api/Post/5
        [HttpGet("{id}", Name = "Get")]
        public async Task<PostModel> GetAsync(int id)
        {
            var Post = await _blogContext.Posts.Include(post => post.Comments).FirstOrDefaultAsync(m => m.ID == id);
            return Post;
        }

        // POST: api/Post
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT: api/Post/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
